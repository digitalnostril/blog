---
title: "Create Free Managed Kubernetes Cluster in Oracle Cloud with Terraform"
description: "How to get a free cluster in the cloud."
pubDate: 'Sept 15 2024'
heroImage: '../../assets/images/create-free-managed-kubernetes-cluster-in-oracle-cloud/2.png'
category: 'Containers'
tags: ['AKS', 'minikube', 'kubernetes']
---

This will guide you through making a free kubernetes cluster on Oracle Cloud using some prepared basic Terraform, to get you up and running quickly.

The networking is based on [Example 3: Cluster with OCI CNI Plugin, Public Kubernetes API Endpoint, Private Worker Nodes, and Public Load Balancers](https://docs.oracle.com/en-us/iaas/Content/ContEng/Concepts/contengnetworkconfigexample.htm#example-oci-cni-publick8sapi_privateworkers_publiclb), using the example values.
Oracle does now provide some [example terraform scripts for networking](https://docs.oracle.com/en-us/iaas/compute-cloud-at-customer/topics/oke/example-terraform-scripts-for-network-resources.htm), but the terraform referenced here was not based on those examples.

## Prerequisites
Make sure you have the following cli tools installed an ready to use:
- kubectl
- Terraform
- Helm CLI

## 1\.Sign up for the Oracle Cloud Free tier
a. [Go here](https://www.oracle.com/ca-en/cloud/free/) to sign up
  - The resources we will be using are under the `Always Free Services`, and will continue to run after the trial, however you will not be able to make changes to the cluster (like updating kubernetes version) unless you upgrade to a `Pay As You Go` account. But since they are `Always Free Services` you will not be billed.

## 2\. Setup OCI CLI
a. [Follow the instructions](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm) to download and install oci-cli for your OS.
- I'm on Windows so I"m downloading the latest release that has an MSI installer from https://github.com/oracle/oci-cli/releases
  - Currently that is `oci-cli-3.45.2-Windows-Server-Installer.msi`

b. After oci-cli is installed run `oci session authenticate --no-browser`
  - Select your region
  - Enter `y` to create a new config file
  - Enter `y` to create config file by logging in via browser
  - Log in to Oracle Cloud on the browser page that opens. Whem authenticated, it will save an API key on your local system.

c. Run `oci iam region list`
  - If you get an authentication error, wait a few minutes and then try again
  - Find the key and name for your region and save them for a following step

## 3\. Create the Cluster
a. Clone this repository of terraform code: https://github.com/digitalnostril/free-oci-k8s
  - This is pre-configured to create a basic tier Oracle Kubernetes cluster and set up the networking as described in [Example 3: Cluster with OCI CNI Plugin, Public Kubernetes API Endpoint, Private Worker Nodes, and Public Load Balancers](https://docs.oracle.com/en-us/iaas/Content/ContEng/Concepts/contengnetworkconfigexample.htm#example-oci-cni-publick8sapi_privateworkers_publiclb)
  - This will be a 4 node Arm64 cluster with 4 cpu and 6gb memory per node, since that is what Oracle offers for Always Free Services

b. In the directory of the cloned repository, create a `terraform.tfvars` file. Add the following into the file:
  - Using the region name retrieved in Step 2.c, add `region = "<region name>"` 
  - Using the region key retrieved in Step 2.c, add `region_identifier = "<region key>"`
  - Run `oci iam compartment list` and grab the `compartment-id`, add `compartment_ocid = "<compartment-id>"`
  - Pick a supported kubernetes version from [here](https://docs.oracle.com/en-us/iaas/Content/ContEng/Concepts/contengaboutk8sversions.htm#supportedk8sversions). Add `kubernetes_version = "v<kubernetes version>"`
  - Find the lastest image from [here](https://docs.oracle.com/en-us/iaas/images/oke-worker-node-oracle-linux-8x/index.htm) that uses that kubernetes version you chose and that is `aarch64` and click on it
  - Grab the ocid for that image from that page, and add `image_id = "<image ocid>"`

c. In the end, your `terraform.tfvars` file should look something like:
```
region = "ca-toronto-1"
region_identifier = "YYZ"
compartment_ocid = "ocid1.tenancy.oc1..aaaaaaaadhshjle9glhghgf86ohldgjk08df8df0"
kubernetes_version = "v1.30.1"
image_id = "ocid1.image.oc1.ca-toronto-1.aaaaaaaai25tbzgbm26iszk6h5b5gsmpztks5xqvfwti3t2k3f2qzhbkoapq"
```

d. On the terminal, change into the directory of the repository and run
```
terraform init
terraform apply
```

e. Confirm to create the resources, once the terraform completes, Oracle cloud will still be working on getting the node pool of the cluster ready.

## 4\. Install Ingress Controller
a. Find your cluster in the Oracle Cloud portal under Kubernetes Clusters and click on it.
  - Go to node pools, click on `pool1` and confirm/wait until nodes are ready
    ![](../../assets/images/create-free-managed-kubernetes-cluster-in-oracle-cloud/1.png)
  - Go back to the cluster details page and select `Access Cluster`
    ![](../../assets/images/create-free-managed-kubernetes-cluster-in-oracle-cloud/2.png)
  - Select Local Access and follow the steps, making sure you run the command for using the public endpoint
    ![](../../assets/images/create-free-managed-kubernetes-cluster-in-oracle-cloud/3.png)
  - Confirm access to the cluster from within your terminal by running `kubectl get po -n kube-system` which should produce results

b. Add the helm repo for ingress-nginx `helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx`

c. Install the ingress-nginx helm chart `helm install ingress-nginx ingress-nginx/ingress-nginx --namespace ingress-nginx --create-namespace --set "controller.service.annotations.oci\.oraclecloud\.com/load-balancer-type=nlb"`
  - This specifically sets an annotation on for the LoadBalancer service so that Oracle Cloud will use a Network Load Balancer

d. I've seen the security rules for the load balancer subnet occaisonally get overrwritten when the network loadbalancer gets created, so run another `terraform apply` to see if the load balancer security rules need to be added again.

e. Run `kubectl get svc -n ingress-nginx ingress-nginx-controller` and get the public IP from the `EXTERNAL-IP` field.
- Open your web browser and hit `http://<public ip here>`. You should receive a 404 Not Found error, but it should identify as coming from nginx. You are now done configuring the Oracle Cloud of things and your ingress controller is ready to be used.

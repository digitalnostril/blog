---
title: "Using Azure CSI Drivers in Minikube"
description: "When testing in minikube, you may desire to be using the same tools that you are using in AKS."
pubDate: 'Jan 03 2024'
heroImage: '../../assets/images/using-azure-csi-drivers-in-minikube/3.png'
category: 'Containers'
tags: ['AKS', 'minikube', 'kubernetes']
---

If you're running an AKS cluster, [you may have the Azure CSI drivers enabled for Azure Disks, Blobs, and Files.](https://learn.microsoft.com/en-us/azure/aks/csi-storage-drivers) 

When testing in minikube, [while Kubernetes has some built in support for Azure Storage](https://kubernetes.io/docs/concepts/storage/storage-classes/#provisioner), you may desire to be using the same tools that you are using in AKS.

The unsupported version of the [Azure File CSI driver](https://github.com/kubernetes-sigs/azurefile-csi-driver) and the [Azure Blob Store CSI driver](https://github.com/kubernetes-sigs/blob-csi-driver) are available to be installed. The Azure Disk CSI driver is available as well, however your node needs to be running on an Azure VM in order for that one to work.

Below, I will show you how to install the Azure File CSI Driver, but the same process will also work for the Azure Blob Storage CSI Driver.

## Prerequisites
- Azure Service Principal
- minikube cluster
- Helm CLI

## Install Azure File CSI driver

### 1\. Set up the config for the service principal
Set up the config for the service principal
[The repo contains instructions for using the account key to access Azure Files.](https://github.com/kubernetes-sigs/azurefile-csi-driver/blob/master/deploy/example/e2e_usage.md#option2-bring-your-own-storage-account-only-for-smb-protocol) But what we're going to do is set up the Azure Files CSI Driver to use a service principal, which isn't as clearly documented.

a. The first thing we need to do is create the json config with the information for our service principal. That looks like this:
```json
{
	cloud: AzurePublicCloud,
	tenantId: <Entra ID tenant ID>,
	subscriptionId: <Subscription ID for Azure Storage Resources>,
	resourceGroup: <Resource Group that dynamically provisioned resouces will be created in>,
	location: <Azure region for resources>,
	aadClientId: <Service Principal ID>,
	aadClientSecret: <Service Principal Secret>,
	useManagedIdentityExtension: false
}	
```
- **subscriptionId:** If you want to access Azure Files not in this subscription, you will have to fallback to using an account key instead.
- **resourceGroup:** Dynamic resources will be created here, but you can specify a different resource group in your StorageClass if you are accessing existing Azure Files. The Service Principal will need to be granted the appropriate permissions to the resource group in either case.

b. Save your config as a file called `cloud-config`

c. Create a secret using the `cloud-config` file
```bash
kubectl create secret generic azure-cloud-provider -n kube-system --from-file=path/to/your/directory/cloud-config
```

![](../../assets/images/using-azure-csi-drivers-in-minikube/1.png)

### 3\. Install the chart's helm repo
```bash
helm repo add azurefile-csi-driver https://raw.githubusercontent.com/kubernetes-sigs/azurefile-csi-driver/master/charts
```

![](../../assets/images/using-azure-csi-drivers-in-minikube/2.png)

### 5\. Install
```bash
helm install azurefile-csi-driver azurefile-csi-driver/azurefile-csi-driver --namespace kube-system --set controller.replicas=1
```

![](../../assets/images/using-azure-csi-drivers-in-minikube/3.png)

Notes:
- This will use the `azure-cloud-provider` secret by default. [If you want to use a different secret name and/or namespace for the secret you can add additional arguments.](https://github.com/kubernetes-sigs/azurefile-csi-driver/tree/master/charts#tips)
- If you're trying to install the Blob driver, you may be tempted to set `enableBlobfuseProxy: true` for seemless updates. But that only works for Debians based nodes, so it will not work with minikube.

### 6\. Testing
To test, we can use some of the example storage classes and persistent volume claim to dynamically provision a storage resource for a pod.

a. Create the storage class
```bash
kubectl create -f https://raw.githubusercontent.com/kubernetes-sigs/azurefile-csi-driver/master/deploy/example/storageclass-azurefile-csi.yaml
```
Anything that uses this storage class with have the storage dynamically created and use the `Premium_LRS` sku.


b. Create the PVC
```bash
kubectl create -f https://raw.githubusercontent.com/kubernetes-sigs/azurefile-csi-driver/master/deploy/example/pvc-azurefile-csi.yaml
```
This will use the `azurefile-csi` storage class

![](../../assets/images/using-azure-csi-drivers-in-minikube/4.png)

c. Create the example pod
```bash
kubectl create -f https://raw.githubusercontent.com/kubernetes-sigs/azurefile-csi-driver/master/deploy/example/nginx-pod-azurefile.yaml
```
This will use the `pvc-azurefile` PVC and mount the storage at `/mnt/azurefile`